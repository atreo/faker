Atreo/Faker
===========================


Installation
------------

```sh
$ composer require atreo/faker
```

add extension configuration before kdyby/console:

```
faker: Atreo\Faker\DI\FakerExtension

faker:
	count: 10 # default count
	entities:
		My\Entity\Name:
			count: 50
			formatters:
				type: Nette\DI\Statement('randomElement', [['free', 'paid']])
				slug: Nette\DI\Statement('slug', [5, false])

		Another\Entity\Name:
			formatters:
				name: Nette\DI\Statement('sentence', [6])
				slug: Nette\DI\Statement('slug', [4])
				isPublic: true
```

Now you can run:

```sh
$ php www/index.php faker:list
$ php www/index.php faker:fake My\Entity\Name 15
```

List of formatters you can find [here](https://github.com/fzaninotto/Faker#formatters).

Also you can use oneByOne:

```sh
type: Nette\DI\Statement('oneByOne', [[1, 2, 3, 4]])
```