<?php

namespace Atreo\Faker;

use Faker\Provider\Base;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class OneByOneProvider extends Base
{

	/**
	 * @var array
	 */
	private static $indexes = [];



	/**
	 * @param array $values
	 * @return mixed
	 */
	public static function oneByOne(array $values)
	{
		$hash = md5(serialize($values));

		if (!isset(self::$indexes[$hash])) {
			self::$indexes[$hash] = 0;
		}

		$index = self::$indexes[$hash];

		if ($index + 1 > count($values) - 1) {
			self::$indexes[$hash] = 0;
		} else {
			self::$indexes[$hash]++;
		}

		return $values[$index];
	}

}
