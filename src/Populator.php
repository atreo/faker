<?php

namespace Atreo\Faker;

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Faker\Factory;
use Faker\ORM\Doctrine\EntityPopulator;
use Kdyby\Doctrine\EntityManager;
use Nette\DI\Statement;
use Nette\Object;
use Nette\Utils\Random;
use Nette\Utils\Strings;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class Populator extends Object
{

	/**
	 * @var \Kdyby\Doctrine\EntityManager
	 */
	private $em;

	/**
	 * @var \Faker\Generator
	 */
	private $generator;

	/**
	 * @var array
	 */
	private $defaultFormatters = [];

	/**
	 * @var ClassMetadata
	 */
	private $metadata;

	/**
	 * @var EntityPopulator
	 */
	private $entity;

	/**
	 * @var array
	 */
	private $entities = [];

	/**
	 * @var array
	 */
	private $quantities = [];

	/**
	 * @var array
	 */
	private $generateId = [];



	/**
	 * @param \Kdyby\Doctrine\EntityManager $em
	 * @param string $locale
	 */
	public function __construct(EntityManager $em, $locale)
	{
		$this->em = $em;
		$this->generator = Factory::create($locale);
		$this->generator->addProvider(new OneByOneProvider($this->generator));

		$this->defaultFormatters = [
			'slug' => function() {
				return $this->generator->slug(6);
			},
			'hash' => function() {
				return Random::generate(20);
			}
		];
	}



	/**
	 * @param mixed $entity
	 * @param integer $number
	 * @param array $customColumnFormatters
	 * @param array $customModifiers
	 * @param boolean $generateId
	 */
	public function addEntity($entity, $number, $customColumnFormatters = [], $customModifiers = [], $generateId = FALSE)
	{
		$this->metadata = $this->em->getClassMetadata($entity);
		$this->entity = new EntityPopulator($this->metadata);

		$guessedFormatters = $this->entity->guessColumnFormatters($this->generator);

		$guessedFormatters = $this->updateGuessedFieldFormatters($guessedFormatters);
		$guessedFormatters = $this->updateGuessedAssociationFormatters($guessedFormatters);
		$this->entity->setColumnFormatters($guessedFormatters);

		$customColumnFormatters = $this->getCustomColumnFormatters($customColumnFormatters);
		$this->entity->mergeColumnFormattersWith($customColumnFormatters);

		$this->entity->mergeModifiersWith($customModifiers);
		$this->generateId[$this->entity->getClass()] = $generateId;

		$class = $this->entity->getClass();
		$this->entities[$class] = $this->entity;
		$this->quantities[$class] = $number;
	}



	private function getCustomColumnFormatters(array $customColumnFormatters = [])
	{
		foreach ($customColumnFormatters as $column => $formatter) {
			$customColumnFormatters[$column] = function() use($column, $formatter) {

				if ($formatter instanceof Statement) {
					$method = $formatter->getEntity();
					$arguments = empty($formatter->arguments) ? NULL : $formatter->arguments;
					$value = call_user_func_array([$this->generator, $method], $arguments);

					try {
						$class = $this->metadata->getAssociationTargetClass($column);
						$usedAssociations[$class] = $class;
						return $this->em->getReference($class, $value);
					} catch (\InvalidArgumentException $e) {
						return $value;
					}
				}

				return $formatter;
			};
		}

		return $customColumnFormatters;
	}



	private function updateGuessedFieldFormatters(array $guessedFormatters = [])
	{
		foreach ($this->metadata->getFieldNames() as $column) {
			if (!isset($guessedFormatters[$column])) { continue; }

			$columnLowered = Strings::lower($column);
			$formatter = $guessedFormatters[$column];

			$mapping = $this->metadata->getFieldMapping($column);
			if (Strings::contains($columnLowered, 'slug')) {
				$guessedFormatters[$column] = $this->defaultFormatters['slug'];
			} elseif (Strings::contains($columnLowered, 'hash') || Strings::contains($columnLowered, 'token')) {
				$guessedFormatters[$column] = $this->defaultFormatters['hash'];
			}

			if ($mapping['unique']) {
				$guessedFormatters[$column] = $this->getUniqueValue($column, $formatter);
			}
		}

		return $guessedFormatters;
	}



	private function updateGuessedAssociationFormatters(array $guessedFormatters = [])
	{
		foreach ($this->metadata->getAssociationMappings() as $associationMapping) {
			$column = $associationMapping['fieldName'];
			if (!isset($guessedFormatters[$column])) { continue; }

			$formatter = $guessedFormatters[$column];

			if (in_array($associationMapping['type'], [ClassMetadataInfo::TO_ONE, ClassMetadataInfo::MANY_TO_ONE, ClassMetadataInfo::ONE_TO_ONE])) {
				$guessedFormatters[$column] = $this->getAssociationValue($associationMapping, $formatter, $associationMapping['type'] == ClassMetadataInfo::ONE_TO_ONE);
			}
		}

		return $guessedFormatters;
	}



	private function getUniqueValue($column, $formatter)
	{
		return function($inserted, $obj) use($column, $formatter) {

			do {
				$value = $formatter($inserted, $obj);
				$exists = $this->em->getRepository($this->entity->getClass())->countBy([$column => $value]);
			} while ($exists);

			return $value;

		};
	}



	/**
	 * @param array $associationMapping
	 * @param \Closure $formatter
	 * @param boolean $unique
	 * @return \Closure
	 */
	private function getAssociationValue(array $associationMapping, $formatter, $unique = FALSE)
	{
		return function($inserted, $obj) use ($associationMapping, $formatter, $unique) {

			$value = $formatter($inserted, $obj);
			if ($value == NULL) {

				if ($unique) {
					throw new \Exception('Cant get unique value');
				}

				$cnt = $this->em->getRepository($associationMapping['targetEntity'])->countBy([]);
				if ($cnt > 0) {

					$value = $this->em->getRepository($associationMapping['targetEntity'])->createQueryBuilder('e')
						->setFirstResult(rand(0, $cnt - 1))
						->setMaxResults(1)
						->getQuery()->getSingleResult();

				}
			}

			return $value;
		};
	}



	/**
	 * @return array A list of the inserted PKs
	 */
	public function execute()
	{
		$insertedEntities = array();
		foreach ($this->quantities as $class => $number) {
			$generateId = $this->generateId[$class];
			for ($i=0; $i < $number; $i++) {
				$insertedEntities[$class][]= $this->entities[$class]->execute($this->em, $insertedEntities, $generateId);
			}
			$this->em->flush();
		}

		return $insertedEntities;
	}

}
