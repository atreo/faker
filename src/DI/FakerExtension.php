<?php

namespace Atreo\Faker\DI;

use Kdyby\Console\DI\ConsoleExtension;
use Nette\DI\CompilerExtension;
use Nette\Utils\Validators;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class FakerExtension extends CompilerExtension
{

	/**
	 * @var array
	 */
	private $default = [
		'count' => 10,
		'entities' => [],
		'locale' => 'cs_CZ'
	];



	public function beforeCompile()
	{
		$config = $this->getConfig($this->default);

		Validators::assertField($config, 'count', 'integer');
		Validators::assertField($config, 'locale', 'string');
		Validators::assertField($config, 'entities', 'array');

		foreach ($config['entities'] as $entity => $options) {
			Validators::assert($entity, 'string');
			if ($options !== NULL) {
				if (!isset($options['formatters'])) {
					continue;
				}

				Validators::assertField($options, 'formatters', 'array');
				foreach ($options['formatters'] as $column => $statement) {
					Validators::assert($column, 'string');
				}
			}
		}

		$builder = $this->getContainerBuilder();

		$builder->addDefinition($this->prefix('faker'))
			->setClass('Atreo\Faker\Faker', [
				$config['count'],
				$config['entities'],
				$config['locale']
			]);

		$builder->addDefinition($this->prefix('fakeCommand'))
			->setClass('Atreo\Faker\FakeCommand')
			->addTag(ConsoleExtension::TAG_COMMAND);

		$builder->addDefinition($this->prefix('listCommand'))
			->setClass('Atreo\Faker\ListCommand')
			->addTag(ConsoleExtension::TAG_COMMAND);

	}

}
