<?php

namespace Atreo\Faker;

use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Faker\Factory;
use Faker\Generator;
use Kdyby\Doctrine\EntityManager;
use Nette\DI\Statement;
use Nette\Object;
use Nette\Utils\Arrays;
use Nette\Utils\Strings;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class Faker extends Object
{

	/**
	 * @var integer
	 */
	private $count;

	/**
	 * @var array
	 */
	private $entities = [];

	/**
	 * @var string
	 */
	private $locale;

	/**
	 * @var EntityManager
	 */
	private $em;

	/**
	 * @var Populator
	 */
	private $populator;



	/**
	 * @param integer $count
	 * @param array $entities
	 * @param string $locale
	 * @param EntityManager $em
	 */
	public function __construct($count, array $entities, $locale, EntityManager $em)
	{
		$this->count = $count;
		$this->entities = $entities;
		$this->locale = $locale;
		$this->em = $em;
		$this->populator = new Populator($this->em, $this->locale);
	}



	/**
	 * @return array
	 */
	public function getEntities()
	{
		return $this->entities;
	}



	/**
	 * @param string $entity
	 * @param integer $count
	 * @param array $formatters
	 * @param array $modifiers
	 * @param bool $generateId
	 * @return array
	 */
	private function addEntity($entity, $count = NULL, array $formatters = [], array $modifiers = [], $generateId = FALSE)
	{
		if ($count === NULL) {
			$count = $this->count;
		}

		$this->populator->addEntity($entity, $count, $formatters, $modifiers, $generateId);
	}



	/**
	 * @param string|integer|NULL $entityNameOrNumber
	 * @param integer $count
	 * @return array Inserted entities
	 */
	public function fake($entityNameOrNumber = NULL, $count = NULL)
	{
		$i = 0;
		foreach ($this->entities as $entity => $config) {
			$i++;

			if ($entityNameOrNumber) {
				if ($entityNameOrNumber != $entity && $entityNameOrNumber != $i) {
					continue;
				}
			}

			if ($config == NULL) {
				$this->addEntity($entity);
			} else {
				$this->addEntity(
					$entity,
					$count ? $count : Arrays::get($config, 'count', $this->count),
					Arrays::get($config, 'formatters', []),
					Arrays::get($config, 'modifiers', []),
					Arrays::get($config, 'generateId', FALSE)
				);
			}
		}

		return $this->populator->execute();
	}

}
