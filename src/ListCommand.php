<?php

namespace Atreo\Faker;

use Kdyby\Console\ContainerHelper;
use Nette\DI\Container;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class ListCommand extends Command
{

	protected function configure()
	{
		$this->setName('faker:list');
	}



	public function execute(InputInterface $input, OutputInterface $output)
	{
		/** @var ContainerHelper $containerHelper */
		$containerHelper = $this->getHelper('container');

		/** @var Container $container */
		$container = $containerHelper->getByType(Container::class);

		/** @var Faker $faker */
		$faker = $container->getByType(Faker::class);

		$i = 0;
		foreach ($faker->getEntities() as $entity => $options) {
			$i++;
			$output->writeln($i . ') ' . $entity);
		}
	}

}
