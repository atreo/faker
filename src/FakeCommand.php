<?php

namespace Atreo\Faker;

use Kdyby\Console\ContainerHelper;
use Nette\DI\Container;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class FakeCommand extends Command
{

	protected function configure()
	{
		$this->setName('faker:fake')
			->addArgument('entity', InputArgument::OPTIONAL, 'Entity number (use faker:list) or name', NULL)
			->addArgument('count', InputArgument::OPTIONAL, 'Count of faked entities', NULL);
	}



	public function execute(InputInterface $input, OutputInterface $output)
	{
		$entity = $input->getArgument('entity');
		$count = $input->getArgument('count');

		/** @var ContainerHelper $containerHelper */
		$containerHelper = $this->getHelper('container');

		/** @var Container $container */
		$container = $containerHelper->getByType(Container::class);

		/** @var Faker $faker */
		$faker = $container->getByType(Faker::class);
		$entities = $faker->fake($entity, $count);

		foreach ($entities as $entityName => $items) {
			$output->writeln(count($items) . ' entities of ' . $entityName . ' added!');
		}

	}

}
